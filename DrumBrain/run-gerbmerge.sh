#!/bin/bash

grep -v -E "^G90$" logic/DrumBrainLogic.drl | grep -v -E "^G05$" | grep -v -E "^T0$" >logic/DrumBrainLogic-gm.drl
grep -v -E "^G90$" acquisition/DrumBrain.drl | grep -v -E "^G05$" | grep -v -E "^T0$" >acquisition/DrumBrain-gm.drl

trap 'echo Ctrl-C pressed !' 2

gerbmerge gerbmerge.cfg gerbmerge.layout

# disable Ctrl-C

echo "Correcting drill file"

# Add tool size definitions to tool def file
while read line; do
    if [ "$line" = "%" ]; then break; fi
    echo $line
done <logic/DrumBrainLogic.drl >merged.drl

#cat merged.toollist.drl merged.drills.drl >> merged.drl
cat merged.drills.drl >> merged.drl
