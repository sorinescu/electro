EESchema Schematic File Version 2  date Du 08 apr 2012 10:53:21 +0300
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:1wire
LIBS:3M
LIBS:7sh
LIBS:20f001n
LIBS:29le010
LIBS:40xx
LIBS:65xxx
LIBS:74ah
LIBS:74hc(t)4046
LIBS:74xx-eu
LIBS:74xx-little-us
LIBS:78xx_with_heatsink
LIBS:648a40-01-tsop40--14mm
LIBS:751xx
LIBS:abi-lcd
LIBS:ad
LIBS:ad620
LIBS:ad624
LIBS:ad633
LIBS:ad725
LIBS:ad7709
LIBS:ad7710
LIBS:ad8018
LIBS:ad9235
LIBS:ad9834
LIBS:ad9851
LIBS:ad22151
LIBS:ad74111_adg3304
LIBS:adns-2610
LIBS:ads11xx
LIBS:aduc816
LIBS:aduc841
LIBS:adv7202
LIBS:advsocket-computer
LIBS:adxl
LIBS:adxl103_203e
LIBS:adxl311
LIBS:adxl320
LIBS:adxrs300
LIBS:agilent-infrared
LIBS:akm
LIBS:allegro
LIBS:alps_blue_rk-27_single
LIBS:altera
LIBS:altera-2
LIBS:altera-cycloneii
LIBS:altera-ep3c40f484
LIBS:amber_bnc_iv
LIBS:AMD_ELAN_SC_520
LIBS:amd-mach
LIBS:amd_taxi
LIBS:am-hrrn-xxx
LIBS:amphenol-simlock
LIBS:anadigm-1
LIBS:analog_device_adxl202e
LIBS:analog-devices
LIBS:analog-devices2
LIBS:analog-devices-current-shunt-monitors
LIBS:analog-devices-dds
LIBS:analog_devices_gaui
LIBS:ap_lna
LIBS:ap_mixernew
LIBS:ap_rc_0402
LIBS:ap_saw
LIBS:ap_transformer
LIBS:ap_vco
LIBS:asix
LIBS:at89x52
LIBS:at90can128
LIBS:at91sam7sxxx-au
LIBS:at91sam9261
LIBS:atarilynx
LIBS:atf1502als
LIBS:atmega8
LIBS:atmel89cxxxx
LIBS:atmel-1
LIBS:atmel-2005
LIBS:atmel_prototype_header
LIBS:attiny13
LIBS:attiny24_44_84
LIBS:aurel
LIBS:avr
LIBS:avr32
LIBS:avr-1
LIBS:avr-2
LIBS:avr-3
LIBS:avr-4
LIBS:axis
LIBS:basicstamp2
LIBS:basic_stamps
LIBS:basicx
LIBS:basicx_chips
LIBS:bluegiga
LIBS:blueradio-br-sc30a
LIBS:bourns
LIBS:burr-brown
LIBS:burr-brown-2
LIBS:burr-brown-3
LIBS:burr-brown-ads8341
LIBS:butterfly9
LIBS:bv_20
LIBS:bx_24
LIBS:californiamicro
LIBS:calmicro
LIBS:Capacitor-Wima-FKM2-100v
LIBS:Capacitor-Wima-FKM2-250v
LIBS:Capacitor-Wima-FKM2-400v
LIBS:Capacitor-Wima-FKM3-160v
LIBS:Capacitor-Wima-FKM3-250v
LIBS:Capacitor-Wima-FKM3-400v
LIBS:Capacitor-Wima-FKP1-400v
LIBS:Capacitor-Wima-FKP1-630v
LIBS:Capacitor-Wima-FKP1-1000v
LIBS:Capacitor-Wima-FKP1-1250v
LIBS:Capacitor-Wima-FKP1-1600v
LIBS:Capacitor-Wima-FKP1-2000v
LIBS:Capacitor-Wima-FKP1-4000v
LIBS:Capacitor-Wima-FKP1-6000v
LIBS:Capacitor-Wima-FKP2-63v
LIBS:Capacitor-Wima-FKP2-100v
LIBS:Capacitor-Wima-FKP2-250v
LIBS:Capacitor-Wima-FKP2-400v
LIBS:Capacitor-Wima-FKP2-630v
LIBS:Capacitor-Wima-FKP2-800v
LIBS:Capacitor-Wima-FKP2-1000v
LIBS:Capacitor-Wima-FKP3-100v
LIBS:Capacitor-Wima-FKP3-160v
LIBS:Capacitor-Wima-FKP3-250v
LIBS:Capacitor-Wima-FKP3-400v
LIBS:Capacitor-Wima-FKP3-630v
LIBS:Capacitor-Wima-FKP3-1000v
LIBS:Capacitor-Wima-FKS2-100v
LIBS:Capacitor-Wima-FKS2-250v
LIBS:Capacitor-Wima-FKS2-400v
LIBS:Capacitor-Wima-FKS3-100v
LIBS:Capacitor-Wima-FKS3-160v
LIBS:Capacitor-Wima-FKS3-250v
LIBS:Capacitor-Wima-FKS3-400v
LIBS:Capacitor-Wima-FKS3-630v
LIBS:Capacitor-Wima-MKM2-100v
LIBS:Capacitor-Wima-MKM2-250v
LIBS:Capacitor-Wima-MKM4-250v
LIBS:Capacitor-Wima-MKM4-400v
LIBS:Capacitor-Wima-MKP2-100v
LIBS:Capacitor-Wima-MKP2-250v
LIBS:Capacitor-Wima-MKP2-400v
LIBS:Capacitor-Wima-MKP2-630v
LIBS:Capacitor-Wima-MKP4-250v
LIBS:Capacitor-Wima-MKP4-400v
LIBS:Capacitor-Wima-MKP4-630v
LIBS:Capacitor-Wima-MKP4-1000v
LIBS:Capacitor-Wima-MKP10-160v
LIBS:Capacitor-Wima-MKP10-250v
LIBS:Capacitor-Wima-MKP10-400v
LIBS:Capacitor-Wima-MKP10-630v
LIBS:Capacitor-Wima-MKP10-1000v
LIBS:Capacitor-Wima-MKP10-1600v
LIBS:Capacitor-Wima-MKP10-2000v
LIBS:Capacitor-Wima-MKS2-16v
LIBS:Capacitor-Wima-MKS2-50v
LIBS:Capacitor-Wima-MKS02-50v
LIBS:Capacitor-Wima-MKS2-63v
LIBS:Capacitor-Wima-MKS02-63v
LIBS:Capacitor-Wima-MKS2-100v
LIBS:Capacitor-Wima-MKS02-100v
LIBS:Capacitor-Wima-MKS2-250v
LIBS:Capacitor-Wima-MKS2-400v
LIBS:Capacitor-Wima-MKS4-50v
LIBS:Capacitor-Wima-MKS4-63v
LIBS:Capacitor-Wima-MKS4-100v
LIBS:Capacitor-Wima-MKS4-250v
LIBS:Capacitor-Wima-MKS4-400v
LIBS:Capacitor-Wima-MKS4-630v
LIBS:Capacitor-Wima-MKS4-1000v
LIBS:Capacitor-Wima-MKS4-1500v
LIBS:Capacitor-Wima-MKS4-2000v
LIBS:Capacitor-Wima-MP3-X2-250v
LIBS:Capacitor-Wima-MP3-X2-275v
LIBS:Capacitor-Wima-MP3-Y2-250v
LIBS:Capacitor-Wima-SMD1812-63v
LIBS:Capacitor-Wima-SMD1812-100v
LIBS:Capacitor-Wima-SMD1812-250v
LIBS:Capacitor-Wima-SMD2020-63v
LIBS:Capacitor-Wima-SMD2020-100v
LIBS:Capacitor-Wima-SMD2020-250v
LIBS:Capacitor-Wima-SMD2824-63v
LIBS:Capacitor-Wima-SMD2824-100v
LIBS:Capacitor-Wima-SMD2824-250v
LIBS:Capacitor-Wima-SMD4036-40v
LIBS:Capacitor-Wima-SMD4036-63v
LIBS:Capacitor-Wima-SMD4036-100v
LIBS:Capacitor-Wima-SMD4036-250v
LIBS:Capacitor-Wima-SMD4036-400v
LIBS:Capacitor-Wima-SMD4036-630v
LIBS:Capacitor-Wima-SMD5045-40v
LIBS:Capacitor-Wima-SMD5045-63v
LIBS:Capacitor-Wima-SMD5045-100v
LIBS:Capacitor-Wima-SMD5045-250v
LIBS:Capacitor-Wima-SMD5045-400v
LIBS:Capacitor-Wima-SMD5045-630v
LIBS:Capacitor-Wima-SMD5045-1000v
LIBS:Capacitor-Wima-SMD6560-40v
LIBS:Capacitor-Wima-SMD6560-63v
LIBS:Capacitor-Wima-SMD6560-100v
LIBS:Capacitor-Wima-SMD6560-250v
LIBS:Capacitor-Wima-SMD6560-400v
LIBS:Capacitor-Wima-SMD6560-630v
LIBS:Capacitor-Wima-SMD6560-1000v
LIBS:Capacitor-Wima-SMD-MP3-Y2-250v
LIBS:CapAudyn
LIBS:Cap-Audyn-Sn
LIBS:Cap-Dreh
LIBS:Cap-Gold
LIBS:Cap-M
LIBS:cap-master
LIBS:Cap-M-Supreme
LIBS:Cap-M-ZN
LIBS:cap-pan
LIBS:cap-pan40
LIBS:CapSiem
LIBS:cap-vishay-153clv
LIBS:c-control
LIBS:chipcon
LIBS:chipcon-ti
LIBS:cirrus
LIBS:cirrus-2
LIBS:cirrus-3
LIBS:cirrus-mpu
LIBS:c-max
LIBS:cml-micro
LIBS:cny70
LIBS:coldfire
LIBS:comfile
LIBS:con-amp
LIBS:con-amphenol
LIBS:con-amp-micromatch
LIBS:con_conec
LIBS:con-cpci
LIBS:con-cuistack
LIBS:con-cypressindustries
LIBS:con-erni
LIBS:con-faston
LIBS:con-fujitsu-pcmcia
LIBS:con-gr47_gsm_module
LIBS:con-harting-ml
LIBS:con-hdrs40
LIBS:con-headers-jp
LIBS:con-hirose
LIBS:con-jack
LIBS:con-jst2
LIBS:con-molex
LIBS:con-molex-2
LIBS:connect
LIBS:con-neutrik_ag
LIBS:conn-hirose
LIBS:con_nokia
LIBS:con-omnetics
LIBS:con-pci_express(pci-e)
LIBS:con-phoenix-250
LIBS:con-phoenix-381_l
LIBS:con-phoenix-500-mstba6
LIBS:con-ptr500+
LIBS:con-pulse
LIBS:conrad
LIBS:con-ria182
LIBS:con-riacon
LIBS:con-subd
LIBS:controller-micro_atmel_jd
LIBS:con-usb
LIBS:con-usb-2
LIBS:con-usb-3
LIBS:converter
LIBS:con-vg
LIBS:con-wago
LIBS:con-yamaichi
LIBS:con-yamaichi-cf
LIBS:con-yamaichi-cf-2
LIBS:crystal
LIBS:crystal-epson
LIBS:cts
LIBS:cy7c1009b
LIBS:cyan
LIBS:cygnal
LIBS:cygnal_mini
LIBS:cypress-fx2
LIBS:cypressmicro
LIBS:dallas
LIBS:dallas600
LIBS:dallas-1
LIBS:dallas-rtc
LIBS:dcsocket
LIBS:dframes
LIBS:dicons
LIBS:digital-transistors
LIBS:diode
LIBS:diode-1
LIBS:dip204b-4nlw
LIBS:dips082
LIBS:display_ea_dog-m
LIBS:display-kingbright
LIBS:display-lcd
LIBS:display-lcd-lxd
LIBS:displayled8x8
LIBS:displaytech_(chr)
LIBS:divers
LIBS:dodolcd
LIBS:dodomicro
LIBS:dome-key
LIBS:dpads
LIBS:dports
LIBS:ds1307_pcf8583
LIBS:ds1813_revised
LIBS:dsp56f8323
LIBS:dsp56f8346
LIBS:dsp_texas
LIBS:ds(s)30655
LIBS:dsymbols
LIBS:ecl
LIBS:edge-156
LIBS:elm
LIBS:Elma
LIBS:ember
LIBS:emerging
LIBS:enfora_module
LIBS:ep_molex_6410_7395-02
LIBS:EP_molex_6410_7395
LIBS:er400trs
LIBS:etx-board
LIBS:ev9200g
LIBS:everlight
LIBS:evision
LIBS:fairchild-tinylogic
LIBS:fetky
LIBS:fichte_div
LIBS:filter-coilcraft
LIBS:fitre
LIBS:flashmemory
LIBS:freescale-accelerometer
LIBS:freescale-mc68hc908jb
LIBS:fsoncore
LIBS:ft8u100ax
LIBS:ft2232c
LIBS:ftdi
LIBS:ftdi2
LIBS:ftdi3
LIBS:ftdi4
LIBS:ftdichip
LIBS:ftdichip-1
LIBS:ftdichip-2
LIBS:ftdichip-3
LIBS:fze1066
LIBS:gameboy
LIBS:gm862
LIBS:gpsms1
LIBS:hacesoft
LIBS:HBridge
LIBS:HD15_B
LIBS:hdsp
LIBS:heatsink-aavid
LIBS:hirose
LIBS:hirose2
LIBS:hirose-fx2-100
LIBS:hirose-fx2-100-2
LIBS:holtek
LIBS:honda2-0
LIBS:HONEYWEL
LIBS:HONY3000
LIBS:HONY4000
LIBS:hopf
LIBS:i2c
LIBS:i2c_95xx
LIBS:ibe_lbr
LIBS:ic-bbd
LIBS:ics512
LIBS:ieee488
LIBS:inductor-coilcraft
LIBS:inductor-nkl
LIBS:inductors-jwmiller
LIBS:inductor-toko
LIBS:infineon
LIBS:infineon_tle_power
LIBS:infineon-tricore
LIBS:intel-strongarm
LIBS:intersil
LIBS:inventronik
LIBS:io-warrior
LIBS:ipc-7351-capacitor
LIBS:ipc-7351-diode
LIBS:ipc-7351-inductor
LIBS:ipc-7351-resistor
LIBS:ipc-7351-transistor
LIBS:ir2112
LIBS:irf
LIBS:irf-2
LIBS:is61c1024al
LIBS:is471f
LIBS:isabellen
LIBS:isd
LIBS:isd-new
LIBS:isd-new-1
LIBS:ite-it81xx
LIBS:jensen
LIBS:jfw-pwm1
LIBS:jst_eph
LIBS:jst-ph
LIBS:jt9s-1p
LIBS:jumper
LIBS:KEY_B
LIBS:keypad4x4_ece
LIBS:keystone
LIBS:kingbright
LIBS:kingfont_sdcmf10915w010
LIBS:kingfont_sdcmf10915w010_covek
LIBS:Knitter-Switch
LIBS:kzh20_lithium_batterie_holder
LIBS:l293e
LIBS:l4960
LIBS:l5973d
LIBS:l6219
LIBS:laipac_technologie
LIBS:lasser
LIBS:lattice
LIBS:lcd_2x16_led_backlight
LIBS:lcd_128x64_led_backlight
LIBS:lcd_modules_ece
LIBS:lcd_n3200_lph7677
LIBS:lcd_nokia_6100
LIBS:lcd_parallel
LIBS:led
LIBS:led_rgb
LIBS:lexx
LIBS:lf2403
LIBS:linear2
LIBS:linear3
LIBS:linear4
LIBS:linear-technology
LIBS:linear-technology-2
LIBS:linear-technology-3
LIBS:linkup-l1110
LIBS:little-logic
LIBS:llk5_library
LIBS:lm393d
LIBS:lm1881
LIBS:lm2937
LIBS:lmd1820x
LIBS:lmh1251
LIBS:lmopamp
LIBS:logo_ru
LIBS:lp2986im-33
LIBS:lp2989-so8
LIBS:lpc210x
LIBS:lpc213x
LIBS:lph-7366
LIBS:ltc6900
LIBS:ltc_swr
LIBS:ltd5122
LIBS:lucorca
LIBS:lumiled
LIBS:lumiled-2
LIBS:luminary-micro
LIBS:lundahl
LIBS:lvds
LIBS:lvds-2
LIBS:matsusada
LIBS:max187
LIBS:max232ti
LIBS:max232ti_n
LIBS:max498
LIBS:max696x
LIBS:max882_883_884
LIBS:max882-smd
LIBS:max1480-90
LIBS:max1678
LIBS:max1811
LIBS:max3235ecwp
LIBS:maxim
LIBS:maxim1719-21
LIBS:maxim-2
LIBS:maxim-3
LIBS:maxim-4
LIBS:maxim-5
LIBS:maxim-6
LIBS:maxim-6a
LIBS:maxim-6b
LIBS:maxim-7
LIBS:maxim-7a
LIBS:maxim-8
LIBS:maxim-9
LIBS:maxim-10
LIBS:maxim-filter
LIBS:mc9s12ne64-lqfp112
LIBS:mc68ez328
LIBS:mc78l08a
LIBS:mc3479p
LIBS:mc33201d
LIBS:mc33202d
LIBS:mc33269
LIBS:mc33269dt
LIBS:mc34063
LIBS:mcp120-130
LIBS:mcp3304
LIBS:memory-bsi
LIBS:memory-intel
LIBS:memory-samsung
LIBS:Mentor-FEL
LIBS:micrel
LIBS:micro-68x_db
LIBS:microchip16c7xx
LIBS:microchip-2
LIBS:microchip-3
LIBS:microchip-4
LIBS:microchip-5
LIBS:microchip-16f688
LIBS:microchip-18f258
LIBS:microchip_can
LIBS:microchip-dspic
LIBS:microchip-enc28j60
LIBS:microchip_enet
LIBS:microchip_mcp2120
LIBS:microchip-mcp125x-xxx
LIBS:microchip_pic16f716
LIBS:microchip-pic18fxx5x
LIBS:microchip-pic24
LIBS:microchip_tc7660
LIBS:micro-codemercenaries
LIBS:micro-fujitsu
LIBS:micro-philips
LIBS:micro-pic18xxx
LIBS:micro-rabbit
LIBS:micro-silabs
LIBS:minicir
LIBS:minicircuits
LIBS:minickts
LIBS:mini_din
LIBS:minipci
LIBS:mitel
LIBS:mitsubishi_mcu
LIBS:mma7260_7261
LIBS:motordrv
LIBS:motorola_hc08
LIBS:motorola-video
LIBS:mp3h6115a
LIBS:mpc5200
LIBS:mpc8250
LIBS:mpx4100a
LIBS:mpxh6400
LIBS:msp430
LIBS:msp430-1
LIBS:mt9t001
LIBS:mt88xx
LIBS:muratanfe61
LIBS:murata-resonators
LIBS:nais_tq2sa_smd_relay
LIBS:nasem_adcs
LIBS:national
LIBS:national-2
LIBS:national_cp3000
LIBS:national_gaui
LIBS:ncp5009
LIBS:nec-upd71055
LIBS:nichia
LIBS:ni_gpib
LIBS:nitron
LIBS:nixie_tube
LIBS:nordic-semi
LIBS:nrf24l01
LIBS:nxp
LIBS:oldchips
LIBS:op-amp
LIBS:opt301m
LIBS:optocoupler
LIBS:optocoupler-2
LIBS:opto-resistor
LIBS:osman-pic16f62x
LIBS:padsim
LIBS:parallax_propeller
LIBS:pbno_m
LIBS:pca82c250
LIBS:pcf8575c
LIBS:PGA2310
LIBS:philips1
LIBS:philips-2
LIBS:philips-3
LIBS:philips-4
LIBS:philips-5
LIBS:philips-6
LIBS:philips_gaui
LIBS:philips_lpc21xx
LIBS:phoenix_gicv-gf
LIBS:phytec
LIBS:pic16f6xx
LIBS:pic16f6xx_14pin
LIBS:pic16f6xx-1
LIBS:pic16f87x
LIBS:pic16f690
LIBS:pic16f818
LIBS:pic16f914
LIBS:pic18f4x2
LIBS:pic18f2320
LIBS:pic18f4550
LIBS:pic18f6680
LIBS:pic18fxx20
LIBS:pic18fxx20-2
LIBS:pic18fxx20-fxx8
LIBS:picaxe-2
LIBS:picaxe-3
LIBS:picogate
LIBS:picolight-msa
LIBS:pictiva
LIBS:piher
LIBS:pinhead
LIBS:pinhead-1
LIBS:pixaxe28x
LIBS:planartransformer
LIBS:platinen
LIBS:pneumatic
LIBS:polyswitch_smd
LIBS:Pot
LIBS:powerconnectorskt
LIBS:Power-in
LIBS:promiesd02
LIBS:propeller
LIBS:pswitch40
LIBS:ptc-littlefuse
LIBS:ptn78060wah
LIBS:pulse-eng
LIBS:ramtron
LIBS:ramtron-1
LIBS:ramtron-2
LIBS:raritaeten
LIBS:RC2000M
LIBS:rcl
LIBS:rcl-2
LIBS:rcm2200
LIBS:rcm3000
LIBS:rcm3360
LIBS:rcm3400_module
LIBS:rc-master
LIBS:rc-master-smd
LIBS:rds-decoder
LIBS:ref-packages
LIBS:relay
LIBS:relay_finder
LIBS:relay-pickering
LIBS:renesas
LIBS:renesas21
LIBS:renesas_mcu
LIBS:renesas_mcu20
LIBS:resistor-bourns
LIBS:resistor-ruf
LIBS:rfm-ash
LIBS:rfm-ash-02
LIBS:rf-micro-devices
LIBS:rfpic
LIBS:rjmg-6xxx-8x-01
LIBS:rtl8019as
LIBS:russian-nixies
LIBS:satcard
LIBS:sawtek
LIBS:scenix4
LIBS:sena_technology-bluetooth
LIBS:sfh511x
LIBS:sfh920x
LIBS:sgs-thom
LIBS:sharp
LIBS:sharp-arm
LIBS:sht10_11_15
LIBS:sht11
LIBS:si-50014
LIBS:sid_max
LIBS:Siemens-Rel
LIBS:silabs
LIBS:silabs-2
LIBS:silabs-eth
LIBS:simmstik
LIBS:sir-tfdu4100_tfds4500_tfdt4500
LIBS:sl6270
LIBS:smartcard
LIBS:smartwed
LIBS:smd-special
LIBS:smsc
LIBS:sn65lvds
LIBS:sn74v293
LIBS:sn2032
LIBS:special-diodes
LIBS:st232_gaui
LIBS:st2221x
LIBS:st7540
LIBS:standingfet
LIBS:stepper
LIBS:st-l6208
LIBS:st-microelectronics
LIBS:st_psd8xxfx
LIBS:st_psd813
LIBS:str711
LIBS:st-tda7375
LIBS:stuart
LIBS:stv_conec_dlsnyyan23x
LIBS:stv_mk3ds1
LIBS:supply
LIBS:supply0
LIBS:supply1
LIBS:switch-copal
LIBS:switch-misc
LIBS:switch-tact
LIBS:sx2_epf10k10a_epc2_c6713
LIBS:t6963
LIBS:tc4sxx-eu
LIBS:tc65_siemens
LIBS:tca0372dw
LIBS:tcm3105
LIBS:tda1562q
LIBS:tda7386
LIBS:tda7496l
LIBS:tda8560q
LIBS:tda8708a
LIBS:termosensor
LIBS:tex
LIBS:tex2
LIBS:texas-msp
LIBS:that2180_sip8
LIBS:that_2252_rms_detector
LIBS:that-corp
LIBS:ti6713bpyp
LIBS:tiger
LIBS:ti_msc1210
LIBS:ti_msp430
LIBS:ti-msp430f14x1
LIBS:tip112
LIBS:ti_scope
LIBS:tlv320aic23b
LIBS:tmc
LIBS:tmp10x
LIBS:tms470
LIBS:tms2402
LIBS:tms2406
LIBS:tms2808
LIBS:tms2810
LIBS:top-switcher
LIBS:toshiba
LIBS:tpic
LIBS:tps769xx
LIBS:tps77633
LIBS:tqm_hydra_xc_modul
LIBS:traco
LIBS:tracop1
LIBS:traco_tmr
LIBS:trafo-xicon
LIBS:transformer-trigger
LIBS:transil
LIBS:transistor
LIBS:transistor-arrays
LIBS:transistor-fet
LIBS:transistor-fet+irf7201
LIBS:transistor-power
LIBS:transistors_gaui
LIBS:Transputers
LIBS:triggered_devices
LIBS:trinamic
LIBS:ts72116tp-10
LIBS:tsop
LIBS:tubes
LIBS:tusb3x10
LIBS:tusb3x10-1
LIBS:tusb6250
LIBS:ua9638
LIBS:ubicom
LIBS:ubicom_v2
LIBS:u-blox
LIBS:ucb1400
LIBS:uc-dimm
LIBS:uicc_card_reader
LIBS:uln-udn
LIBS:ultrasonic_transducer
LIBS:ultrasonic_transducer-1
LIBS:unipac
LIBS:valves2
LIBS:varistor
LIBS:vishay
LIBS:vishay-1
LIBS:vishay_tsal4400_bpw85c
LIBS:v-reg
LIBS:v-reg-2
LIBS:v-reg-fairchild
LIBS:v-reg-lowdrop
LIBS:vs10xx
LIBS:vs1001
LIBS:waveform
LIBS:western-digital-devices
LIBS:wima_c
LIBS:winbond-w9021x
LIBS:wolfson
LIBS:wolleatmel
LIBS:wuerth_elektronik_v5
LIBS:wuerth-elektronik_v4
LIBS:xbee_r1
LIBS:xc2v1000fg456
LIBS:xc3s200-tq144
LIBS:xdr-dram-toshiba
LIBS:xenpak-msa
LIBS:xicor-eepot
LIBS:xilinx_spartan3
LIBS:xilinx_spartan3_virtex4_and_5
LIBS:Xilinx_VirtexII
LIBS:Xilinx_VirtexII_Pro
LIBS:xilinx_virtexii-xc2v80&flashprom
LIBS:xilinx_xc9572xl-tq100
LIBS:xilinx-xc3sxxxe_vq100
LIBS:xport
LIBS:x-port
LIBS:xtx-board
LIBS:z8encore8k_v10
LIBS:zettl
LIBS:Zilog-eZ80-v1_0
LIBS:zilog-z8-encore-v1_2a
LIBS:Zilog-Z8-Encore-v1_1
LIBS:Zilog-ZNEO-v1_0
LIBS:zivapc
LIBS:zx4125p
LIBS:DrumBrain-cache
EELAYER 25  0
EELAYER END
$Descr A4 11700 8267
encoding utf-8
Sheet 1 1
Title ""
Date "8 apr 2012"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 4400 1350 0    60   ~ 0
ADC7
Text Label 4400 1450 0    60   ~ 0
ADC6
Text Label 4400 1550 0    60   ~ 0
ADC5
Text Label 4400 1650 0    60   ~ 0
ADC4
Wire Wire Line
	4900 1650 4150 1650
Wire Wire Line
	4900 1450 4150 1450
Wire Bus Line
	5000 5750 5000 1300
Connection ~ 6000 1250
Wire Wire Line
	6000 1250 6000 1400
Connection ~ 3600 1250
Wire Wire Line
	3850 1250 3400 1250
Wire Wire Line
	3400 1250 3400 1200
Wire Wire Line
	3100 5800 2600 5800
Wire Wire Line
	3100 3400 2600 3400
Wire Wire Line
	3300 1650 3850 1650
Wire Wire Line
	3300 1450 3850 1450
Wire Bus Line
	3200 1250 3200 6200
Wire Wire Line
	3600 1250 3600 900 
Wire Wire Line
	750  1350 750  1450
Wire Wire Line
	1300 5550 1300 5450
Wire Wire Line
	1100 6100 1100 6000
Wire Wire Line
	1100 6000 850  6000
Wire Wire Line
	1300 6250 1300 6350
Wire Wire Line
	1500 5700 1500 5750
Wire Wire Line
	2000 5400 2000 5700
Wire Wire Line
	2500 5400 2600 5400
Wire Wire Line
	2600 5400 2600 5800
Wire Wire Line
	2000 5900 850  5900
Connection ~ 1300 5900
Wire Wire Line
	1300 6050 1300 5750
Wire Wire Line
	1300 4550 1300 4850
Connection ~ 1300 4700
Wire Wire Line
	850  4700 2000 4700
Wire Wire Line
	2600 4200 2600 4600
Wire Wire Line
	2600 4200 2500 4200
Wire Wire Line
	2000 4200 2000 4500
Wire Wire Line
	1500 4500 1500 4550
Wire Wire Line
	1300 5050 1300 5150
Wire Wire Line
	850  4800 1100 4800
Wire Wire Line
	1100 4800 1100 4900
Wire Wire Line
	1300 4350 1300 4250
Wire Wire Line
	1300 3150 1300 3050
Wire Wire Line
	1100 3700 1100 3600
Wire Wire Line
	1100 3600 850  3600
Wire Wire Line
	1300 3850 1300 3950
Wire Wire Line
	1500 3300 1500 3350
Wire Wire Line
	2000 3000 2000 3300
Wire Wire Line
	2500 3000 2600 3000
Wire Wire Line
	2600 3000 2600 3400
Wire Wire Line
	2000 3500 850  3500
Connection ~ 1300 3500
Wire Wire Line
	1300 3650 1300 3350
Wire Wire Line
	1300 2150 1300 2450
Connection ~ 1300 2300
Wire Wire Line
	850  2300 2000 2300
Wire Wire Line
	2600 1800 2600 2200
Wire Wire Line
	2600 1800 2500 1800
Wire Wire Line
	2000 1800 2000 2100
Wire Wire Line
	1500 2100 1500 2150
Wire Wire Line
	1300 2650 1300 2750
Wire Wire Line
	850  2400 1100 2400
Wire Wire Line
	1100 2400 1100 2500
Wire Wire Line
	1300 1950 1300 1850
Wire Wire Line
	750  650  750  750 
Wire Wire Line
	3300 1350 3850 1350
Wire Wire Line
	3300 1550 3850 1550
Wire Wire Line
	2600 2200 3100 2200
Wire Wire Line
	2600 4600 3100 4600
Wire Wire Line
	4150 1250 6200 1250
Wire Wire Line
	5600 950  3600 950 
Connection ~ 3600 950 
Connection ~ 5600 1250
Wire Wire Line
	5100 4600 5600 4600
Wire Wire Line
	5100 2200 5600 2200
Wire Wire Line
	1300 650  1300 750 
Wire Wire Line
	6900 1950 6900 1850
Wire Wire Line
	7100 2500 7100 2400
Wire Wire Line
	7100 2400 7350 2400
Wire Wire Line
	6900 2650 6900 2750
Wire Wire Line
	6700 2100 6700 2150
Wire Wire Line
	6200 1800 6200 2100
Wire Wire Line
	5700 1800 5600 1800
Wire Wire Line
	5600 1800 5600 2200
Wire Wire Line
	7350 2300 6200 2300
Connection ~ 6900 2300
Wire Wire Line
	6900 2150 6900 2450
Wire Wire Line
	6900 3650 6900 3350
Connection ~ 6900 3500
Wire Wire Line
	6200 3500 7350 3500
Wire Wire Line
	5600 3000 5600 3400
Wire Wire Line
	5600 3000 5700 3000
Wire Wire Line
	6200 3000 6200 3300
Wire Wire Line
	6700 3300 6700 3350
Wire Wire Line
	6900 3850 6900 3950
Wire Wire Line
	7350 3600 7100 3600
Wire Wire Line
	7100 3600 7100 3700
Wire Wire Line
	6900 3150 6900 3050
Wire Wire Line
	6900 4350 6900 4250
Wire Wire Line
	7100 4900 7100 4800
Wire Wire Line
	7100 4800 7350 4800
Wire Wire Line
	6900 5050 6900 5150
Wire Wire Line
	6700 4500 6700 4550
Wire Wire Line
	6200 4200 6200 4500
Wire Wire Line
	5700 4200 5600 4200
Wire Wire Line
	5600 4200 5600 4600
Wire Wire Line
	7350 4700 6200 4700
Connection ~ 6900 4700
Wire Wire Line
	6900 4550 6900 4850
Wire Wire Line
	6900 6050 6900 5750
Connection ~ 6900 5900
Wire Wire Line
	6200 5900 7350 5900
Wire Wire Line
	5600 5400 5600 5800
Wire Wire Line
	5600 5400 5700 5400
Wire Wire Line
	6200 5400 6200 5700
Wire Wire Line
	6700 5700 6700 5750
Wire Wire Line
	6900 6250 6900 6350
Wire Wire Line
	7350 6000 7100 6000
Wire Wire Line
	7100 6000 7100 6100
Wire Wire Line
	6900 5550 6900 5450
Wire Wire Line
	1300 1350 1300 1450
Wire Wire Line
	5600 3400 5100 3400
Wire Wire Line
	5600 5800 5100 5800
Wire Wire Line
	4150 1350 4900 1350
Wire Wire Line
	4900 1550 4150 1550
Entry Wire Line
	4900 1350 5000 1450
Entry Wire Line
	4900 1650 5000 1750
Entry Wire Line
	4900 1550 5000 1650
Entry Wire Line
	4900 1450 5000 1550
Entry Wire Line
	5000 5700 5100 5800
Entry Wire Line
	5000 4500 5100 4600
Entry Wire Line
	5000 2100 5100 2200
Entry Wire Line
	5000 3300 5100 3400
$Comp
L TLC274D IC2
U 1 1 4F813A22
P 5900 2200
F 0 "IC2" H 6000 2325 50  0000 L BNN
F 1 "TLC274D" H 6000 2000 50  0000 L BNN
F 2 "linear2-SO14" H 5900 2350 50  0001 C CNN
	1    5900 2200
	-1   0    0    -1  
$EndComp
$Comp
L R R13
U 1 1 4F813A21
P 6450 2100
F 0 "R13" V 6530 2100 50  0000 C CNN
F 1 "470K" V 6450 2100 50  0000 C CNN
	1    6450 2100
	0    -1   1    0   
$EndComp
$Comp
L R R9
U 1 1 4F813A20
P 5950 1800
F 0 "R9" V 6030 1800 50  0000 C CNN
F 1 "470K" V 5950 1800 50  0000 C CNN
	1    5950 1800
	0    -1   1    0   
$EndComp
$Comp
L AGND #PWR01
U 1 1 4F813A1F
P 6700 2150
F 0 "#PWR01" H 6700 2150 40  0001 C CNN
F 1 "AGND" H 6750 2050 50  0000 C CNN
	1    6700 2150
	-1   0    0    -1  
$EndComp
$Comp
L AGND #PWR02
U 1 1 4F813A1E
P 6900 2750
F 0 "#PWR02" H 6900 2750 40  0001 C CNN
F 1 "AGND" H 6900 2650 50  0000 C CNN
	1    6900 2750
	-1   0    0    -1  
$EndComp
$Comp
L +3.3V #PWR03
U 1 1 4F813A1D
P 6900 1850
F 0 "#PWR03" H 6900 1810 30  0001 C CNN
F 1 "+3.3V" H 6900 1960 30  0000 C CNN
	1    6900 1850
	-1   0    0    -1  
$EndComp
$Comp
L PINHD-1X2 JP6
U 1 1 4F813A1C
P 7450 2400
F 0 "JP6" H 7200 2625 50  0000 L BNN
F 1 "to POT 1M" H 7200 2200 50  0000 L BNN
F 2 "pinhead-1X02" H 7450 2550 50  0001 C CNN
	1    7450 2400
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR04
U 1 1 4F813A1B
P 7100 2500
F 0 "#PWR04" H 7100 2500 40  0001 C CNN
F 1 "AGND" H 7100 2300 50  0000 C CNN
	1    7100 2500
	-1   0    0    -1  
$EndComp
$Comp
L SCHOTTKY-DIODESMD D9
U 1 1 4F813A1A
P 6900 2050
F 0 "D9" H 6810 2125 50  0000 L BNN
F 1 "SCHOTTKY" H 6810 1915 50  0000 L BNN
F 2 "diode-SMB" H 6900 2200 50  0001 C CNN
	1    6900 2050
	0    1    -1   0   
$EndComp
$Comp
L SCHOTTKY-DIODESMD D?
U 1 1 4F813A19
P 6900 2550
AR Path="/4F7EBD43" Ref="D?"  Part="1" 
AR Path="/4F7EBD8E" Ref="D?"  Part="1" 
AR Path="/4F813A19" Ref="D10"  Part="1" 
F 0 "D10" H 6810 2625 50  0000 L BNN
F 1 "SCHOTTKY" H 6810 2415 50  0000 L BNN
F 2 "MiniMelf" H 6760 2675 60  0001 C CNN
	1    6900 2550
	0    1    -1   0   
$EndComp
$Comp
L SCHOTTKY-DIODESMD D?
U 1 1 4F813A18
P 6900 3750
AR Path="/4F7EBD43" Ref="D?"  Part="1" 
AR Path="/4F7EBD8E" Ref="D?"  Part="1" 
AR Path="/4F7EBE98" Ref="D?"  Part="1" 
AR Path="/4F813A18" Ref="D12"  Part="1" 
F 0 "D12" H 6810 3825 50  0000 L BNN
F 1 "SCHOTTKY" H 6810 3615 50  0000 L BNN
F 2 "diode-SMB" H 6900 3900 50  0001 C CNN
	1    6900 3750
	0    1    -1   0   
$EndComp
$Comp
L SCHOTTKY-DIODESMD D?
U 1 1 4F813A17
P 6900 3250
AR Path="/4F7EBD43" Ref="D?"  Part="1" 
AR Path="/4F7EBE99" Ref="D?"  Part="1" 
AR Path="/4F813A17" Ref="D11"  Part="1" 
F 0 "D11" H 6810 3325 50  0000 L BNN
F 1 "SCHOTTKY" H 6810 3115 50  0000 L BNN
F 2 "diode-SMB" H 6900 3400 50  0001 C CNN
	1    6900 3250
	0    1    -1   0   
$EndComp
$Comp
L AGND #PWR05
U 1 1 4F813A16
P 7100 3700
F 0 "#PWR05" H 7100 3700 40  0001 C CNN
F 1 "AGND" H 7100 3500 50  0000 C CNN
	1    7100 3700
	-1   0    0    -1  
$EndComp
$Comp
L PINHD-1X2 JP7
U 1 1 4F813A15
P 7450 3600
F 0 "JP7" H 7200 3825 50  0000 L BNN
F 1 "to POT 1M" H 7200 3400 50  0000 L BNN
F 2 "pinhead-1X02" H 7450 3750 50  0001 C CNN
	1    7450 3600
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR06
U 1 1 4F813A14
P 6900 3050
F 0 "#PWR06" H 6900 3010 30  0001 C CNN
F 1 "+3.3V" H 6900 3160 30  0000 C CNN
	1    6900 3050
	-1   0    0    -1  
$EndComp
$Comp
L AGND #PWR07
U 1 1 4F813A13
P 6900 3950
F 0 "#PWR07" H 6900 3950 40  0001 C CNN
F 1 "AGND" H 6900 3850 50  0000 C CNN
	1    6900 3950
	-1   0    0    -1  
$EndComp
$Comp
L AGND #PWR08
U 1 1 4F813A12
P 6700 3350
F 0 "#PWR08" H 6700 3350 40  0001 C CNN
F 1 "AGND" H 6750 3250 50  0000 C CNN
	1    6700 3350
	-1   0    0    -1  
$EndComp
$Comp
L R R10
U 1 1 4F813A11
P 5950 3000
F 0 "R10" V 6030 3000 50  0000 C CNN
F 1 "470K" V 5950 3000 50  0000 C CNN
	1    5950 3000
	0    -1   1    0   
$EndComp
$Comp
L R R14
U 1 1 4F813A10
P 6450 3300
F 0 "R14" V 6530 3300 50  0000 C CNN
F 1 "470K" V 6450 3300 50  0000 C CNN
	1    6450 3300
	0    -1   1    0   
$EndComp
$Comp
L TLC274D IC2
U 2 1 4F813A0F
P 5900 3400
F 0 "IC2" H 6000 3525 50  0000 L BNN
F 1 "TLC274D" H 6000 3200 50  0000 L BNN
F 2 "linear2-SO14" H 5900 3550 50  0001 C CNN
	2    5900 3400
	-1   0    0    -1  
$EndComp
$Comp
L TLC274D IC2
U 3 1 4F813A0E
P 5900 4600
F 0 "IC2" H 6000 4725 50  0000 L BNN
F 1 "TLC274D" H 6000 4400 50  0000 L BNN
F 2 "linear2-SO14" H 5900 4750 50  0001 C CNN
	3    5900 4600
	-1   0    0    -1  
$EndComp
$Comp
L R R15
U 1 1 4F813A0D
P 6450 4500
F 0 "R15" V 6530 4500 50  0000 C CNN
F 1 "470K" V 6450 4500 50  0000 C CNN
	1    6450 4500
	0    -1   1    0   
$EndComp
$Comp
L R R11
U 1 1 4F813A0C
P 5950 4200
F 0 "R11" V 6030 4200 50  0000 C CNN
F 1 "470K" V 5950 4200 50  0000 C CNN
	1    5950 4200
	0    -1   1    0   
$EndComp
$Comp
L AGND #PWR09
U 1 1 4F813A0B
P 6700 4550
F 0 "#PWR09" H 6700 4550 40  0001 C CNN
F 1 "AGND" H 6750 4450 50  0000 C CNN
	1    6700 4550
	-1   0    0    -1  
$EndComp
$Comp
L AGND #PWR010
U 1 1 4F813A0A
P 6900 5150
F 0 "#PWR010" H 6900 5150 40  0001 C CNN
F 1 "AGND" H 6900 5050 50  0000 C CNN
	1    6900 5150
	-1   0    0    -1  
$EndComp
$Comp
L +3.3V #PWR011
U 1 1 4F813A09
P 6900 4250
F 0 "#PWR011" H 6900 4210 30  0001 C CNN
F 1 "+3.3V" H 6900 4360 30  0000 C CNN
	1    6900 4250
	-1   0    0    -1  
$EndComp
$Comp
L PINHD-1X2 JP8
U 1 1 4F813A08
P 7450 4800
F 0 "JP8" H 7200 5025 50  0000 L BNN
F 1 "to POT 1M" H 7200 4600 50  0000 L BNN
F 2 "pinhead-1X02" H 7450 4950 50  0001 C CNN
	1    7450 4800
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR012
U 1 1 4F813A07
P 7100 4900
F 0 "#PWR012" H 7100 4900 40  0001 C CNN
F 1 "AGND" H 7100 4700 50  0000 C CNN
	1    7100 4900
	-1   0    0    -1  
$EndComp
$Comp
L SCHOTTKY-DIODESMD D?
U 1 1 4F813A06
P 6900 4450
AR Path="/4F7EBD43" Ref="D?"  Part="1" 
AR Path="/4F7EBEBE" Ref="D?"  Part="1" 
AR Path="/4F813A06" Ref="D13"  Part="1" 
F 0 "D13" H 6810 4525 50  0000 L BNN
F 1 "SCHOTTKY" H 6810 4315 50  0000 L BNN
F 2 "diode-SMB" H 6900 4600 50  0001 C CNN
	1    6900 4450
	0    1    -1   0   
$EndComp
$Comp
L SCHOTTKY-DIODESMD D?
U 1 1 4F813A05
P 6900 4950
AR Path="/4F7EBD43" Ref="D?"  Part="1" 
AR Path="/4F7EBD8E" Ref="D?"  Part="1" 
AR Path="/4F7EBEBF" Ref="D?"  Part="1" 
AR Path="/4F813A05" Ref="D14"  Part="1" 
F 0 "D14" H 6810 5025 50  0000 L BNN
F 1 "SCHOTTKY" H 6810 4815 50  0000 L BNN
F 2 "diode-SMB" H 6900 5100 50  0001 C CNN
	1    6900 4950
	0    1    -1   0   
$EndComp
$Comp
L SCHOTTKY-DIODESMD D?
U 1 1 4F813A04
P 6900 6150
AR Path="/4F7EBD43" Ref="D?"  Part="1" 
AR Path="/4F7EBD8E" Ref="D?"  Part="1" 
AR Path="/4F7EBED1" Ref="D?"  Part="1" 
AR Path="/4F813A04" Ref="D16"  Part="1" 
F 0 "D16" H 6810 6225 50  0000 L BNN
F 1 "SCHOTTKY" H 6810 6015 50  0000 L BNN
F 2 "diode-SMB" H 6900 6300 50  0001 C CNN
	1    6900 6150
	0    1    -1   0   
$EndComp
$Comp
L SCHOTTKY-DIODESMD D?
U 1 1 4F813A03
P 6900 5650
AR Path="/4F7EBD43" Ref="D?"  Part="1" 
AR Path="/4F7EBED2" Ref="D?"  Part="1" 
AR Path="/4F813A03" Ref="D15"  Part="1" 
F 0 "D15" H 6810 5725 50  0000 L BNN
F 1 "SCHOTTKY" H 6810 5515 50  0000 L BNN
F 2 "diode-SMB" H 6900 5800 50  0001 C CNN
	1    6900 5650
	0    1    -1   0   
$EndComp
$Comp
L AGND #PWR013
U 1 1 4F813A02
P 7100 6100
F 0 "#PWR013" H 7100 6100 40  0001 C CNN
F 1 "AGND" H 7100 5900 50  0000 C CNN
	1    7100 6100
	-1   0    0    -1  
$EndComp
$Comp
L PINHD-1X2 JP9
U 1 1 4F813A01
P 7450 6000
F 0 "JP9" H 7200 6225 50  0000 L BNN
F 1 "to POT 1M" H 7200 5800 50  0000 L BNN
F 2 "pinhead-1X02" H 7450 6150 50  0001 C CNN
	1    7450 6000
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR014
U 1 1 4F813A00
P 6900 5450
F 0 "#PWR014" H 6900 5410 30  0001 C CNN
F 1 "+3.3V" H 6900 5560 30  0000 C CNN
	1    6900 5450
	-1   0    0    -1  
$EndComp
$Comp
L AGND #PWR015
U 1 1 4F8139FF
P 6900 6350
F 0 "#PWR015" H 6900 6350 40  0001 C CNN
F 1 "AGND" H 6900 6250 50  0000 C CNN
	1    6900 6350
	-1   0    0    -1  
$EndComp
$Comp
L AGND #PWR016
U 1 1 4F8139FE
P 6700 5750
F 0 "#PWR016" H 6700 5750 40  0001 C CNN
F 1 "AGND" H 6750 5650 50  0000 C CNN
	1    6700 5750
	-1   0    0    -1  
$EndComp
$Comp
L R R12
U 1 1 4F8139FD
P 5950 5400
F 0 "R12" V 6030 5400 50  0000 C CNN
F 1 "470K" V 5950 5400 50  0000 C CNN
	1    5950 5400
	0    -1   1    0   
$EndComp
$Comp
L R R16
U 1 1 4F8139FC
P 6450 5700
F 0 "R16" V 6530 5700 50  0000 C CNN
F 1 "470K" V 6450 5700 50  0000 C CNN
	1    6450 5700
	0    -1   1    0   
$EndComp
$Comp
L TLC274D IC2
U 4 1 4F8139FB
P 5900 5800
F 0 "IC2" H 6000 5925 50  0000 L BNN
F 1 "TLC274D" H 6000 5600 50  0000 L BNN
F 2 "linear2-SO14" H 5900 5950 50  0001 C CNN
	4    5900 5800
	-1   0    0    -1  
$EndComp
$Comp
L TLC274D IC2
U 5 1 4F8139FA
P 1300 1050
F 0 "IC2" H 1400 1175 50  0000 L BNN
F 1 "TLC274D" H 1400 850 50  0000 L BNN
F 2 "linear2-SO14" H 1300 1200 50  0001 C CNN
	5    1300 1050
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR017
U 1 1 4F8139F9
P 1300 1450
F 0 "#PWR017" H 1300 1450 40  0001 C CNN
F 1 "AGND" H 1300 1250 50  0000 C CNN
	1    1300 1450
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR018
U 1 1 4F8139F8
P 1300 650
F 0 "#PWR018" H 1300 610 30  0001 C CNN
F 1 "+3.3V" H 1300 760 30  0000 C CNN
	1    1300 650 
	1    0    0    -1  
$EndComp
Text Label 5172 4600 0    60   ~ 0
ADC6
Text Label 5172 5800 0    60   ~ 0
ADC7
Text Label 5172 2200 0    60   ~ 0
ADC4
Text Label 5172 3400 0    60   ~ 0
ADC5
$Comp
L CPOL-EUE2.5-5 C1
U 1 1 4F7F0733
P 5600 1050
F 0 "C1" H 5645 1069 50  0000 L BNN
F 1 "CPOL-EUE2.5-5" H 5645 869 50  0000 L BNN
F 2 "rcl-E2,5-5" H 5600 1200 50  0001 C CNN
	1    5600 1050
	1    0    0    -1  
$EndComp
$Comp
L PINHD-2X5 JP5
U 1 1 4F7EE8ED
P 3950 1450
F 0 "JP5" H 3700 1775 50  0000 L BNN
F 1 "uC and PWR" H 3700 1050 50  0000 L BNN
F 2 "pinhead-2X05" H 3950 1600 50  0001 C CNN
	1    3950 1450
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG019
U 1 1 4F7EED1B
P 6200 1250
F 0 "#FLG019" H 6200 1520 30  0001 C CNN
F 1 "PWR_FLAG" H 6200 1480 30  0000 C CNN
	1    6200 1250
	-1   0    0    1   
$EndComp
$Comp
L PWR_FLAG #FLG020
U 1 1 4F7EECD9
P 3400 1200
F 0 "#FLG020" H 3400 1470 30  0001 C CNN
F 1 "PWR_FLAG" H 3400 1430 30  0000 C CNN
	1    3400 1200
	1    0    0    -1  
$EndComp
Text Label 3350 1650 0    60   ~ 0
ADC3
Text Label 3350 1550 0    60   ~ 0
ADC2
Text Label 3350 1450 0    60   ~ 0
ADC1
Text Label 3350 1350 0    60   ~ 0
ADC0
Text Label 2800 3400 0    60   ~ 0
ADC3
Text Label 2800 2200 0    60   ~ 0
ADC2
Text Label 2800 5800 0    60   ~ 0
ADC1
Text Label 2800 4600 0    60   ~ 0
ADC0
Entry Wire Line
	3200 1750 3300 1650
Entry Wire Line
	3200 1650 3300 1550
Entry Wire Line
	3200 1550 3300 1450
Entry Wire Line
	3200 1450 3300 1350
Entry Wire Line
	3100 5800 3200 5900
Entry Wire Line
	3100 4600 3200 4700
Entry Wire Line
	3100 3400 3200 3500
Entry Wire Line
	3100 2200 3200 2300
$Comp
L AGND #PWR021
U 1 1 4F7EE908
P 6000 1400
F 0 "#PWR021" H 6000 1400 40  0001 C CNN
F 1 "AGND" H 6000 1300 50  0000 C CNN
	1    6000 1400
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR022
U 1 1 4F7EE901
P 3600 900
F 0 "#PWR022" H 3600 860 30  0001 C CNN
F 1 "+3.3V" H 3600 1010 30  0000 C CNN
	1    3600 900 
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR023
U 1 1 4F7EBF75
P 750 650
F 0 "#PWR023" H 750 610 30  0001 C CNN
F 1 "+3.3V" H 750 760 30  0000 C CNN
	1    750  650 
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR024
U 1 1 4F7EBF6C
P 750 1450
F 0 "#PWR024" H 750 1450 40  0001 C CNN
F 1 "AGND" H 750 1250 50  0000 C CNN
	1    750  1450
	1    0    0    -1  
$EndComp
$Comp
L TLC274D IC1
U 5 1 4F7EBF25
P 750 1050
F 0 "IC1" H 850 1175 50  0000 L BNN
F 1 "TLC274D" H 850 850 50  0000 L BNN
F 2 "linear2-SO14" H 750 1200 50  0001 C CNN
	5    750  1050
	1    0    0    -1  
$EndComp
$Comp
L TLC274D IC1
U 4 1 4F7EBEDA
P 2300 5800
F 0 "IC1" H 2400 5925 50  0000 L BNN
F 1 "TLC274D" H 2400 5600 50  0000 L BNN
F 2 "linear2-SO14" H 2300 5950 50  0001 C CNN
	4    2300 5800
	1    0    0    -1  
$EndComp
$Comp
L R R5
U 1 1 4F7EBED9
P 1750 5700
F 0 "R5" V 1830 5700 50  0000 C CNN
F 1 "470K" V 1750 5700 50  0000 C CNN
	1    1750 5700
	0    1    1    0   
$EndComp
$Comp
L R R8
U 1 1 4F7EBED8
P 2250 5400
F 0 "R8" V 2330 5400 50  0000 C CNN
F 1 "470K" V 2250 5400 50  0000 C CNN
	1    2250 5400
	0    1    1    0   
$EndComp
$Comp
L AGND #PWR025
U 1 1 4F7EBED7
P 1500 5750
F 0 "#PWR025" H 1500 5750 40  0001 C CNN
F 1 "AGND" H 1550 5650 50  0000 C CNN
	1    1500 5750
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR026
U 1 1 4F7EBED6
P 1300 6350
F 0 "#PWR026" H 1300 6350 40  0001 C CNN
F 1 "AGND" H 1300 6250 50  0000 C CNN
	1    1300 6350
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR027
U 1 1 4F7EBED5
P 1300 5450
F 0 "#PWR027" H 1300 5410 30  0001 C CNN
F 1 "+3.3V" H 1300 5560 30  0000 C CNN
	1    1300 5450
	1    0    0    -1  
$EndComp
$Comp
L PINHD-1X2 JP4
U 1 1 4F7EBED4
P 750 6000
F 0 "JP4" H 500 6225 50  0000 L BNN
F 1 "to POT 1M" H 500 5800 50  0000 L BNN
F 2 "pinhead-1X02" H 750 6150 50  0001 C CNN
	1    750  6000
	-1   0    0    -1  
$EndComp
$Comp
L AGND #PWR028
U 1 1 4F7EBED3
P 1100 6100
F 0 "#PWR028" H 1100 6100 40  0001 C CNN
F 1 "AGND" H 1100 5900 50  0000 C CNN
	1    1100 6100
	1    0    0    -1  
$EndComp
$Comp
L SCHOTTKY-DIODESMD D?
U 1 1 4F7EBED2
P 1300 5650
AR Path="/4F7EBD43" Ref="D?"  Part="1" 
AR Path="/4F7EBED2" Ref="D7"  Part="1" 
F 0 "D7" H 1210 5725 50  0000 L BNN
F 1 "SCHOTTKY" H 1210 5515 50  0000 L BNN
F 2 "diode-SMB" H 1300 5800 50  0001 C CNN
	1    1300 5650
	0    -1   -1   0   
$EndComp
$Comp
L SCHOTTKY-DIODESMD D?
U 1 1 4F7EBED1
P 1300 6150
AR Path="/4F7EBD43" Ref="D?"  Part="1" 
AR Path="/4F7EBD8E" Ref="D?"  Part="1" 
AR Path="/4F7EBED1" Ref="D8"  Part="1" 
F 0 "D8" H 1210 6225 50  0000 L BNN
F 1 "SCHOTTKY" H 1210 6015 50  0000 L BNN
F 2 "diode-SMB" H 1300 6300 50  0001 C CNN
	1    1300 6150
	0    -1   -1   0   
$EndComp
$Comp
L SCHOTTKY-DIODESMD D?
U 1 1 4F7EBEBF
P 1300 4950
AR Path="/4F7EBD43" Ref="D?"  Part="1" 
AR Path="/4F7EBD8E" Ref="D?"  Part="1" 
AR Path="/4F7EBEBF" Ref="D6"  Part="1" 
F 0 "D6" H 1210 5025 50  0000 L BNN
F 1 "SCHOTTKY" H 1210 4815 50  0000 L BNN
F 2 "diode-SMB" H 1300 5100 50  0001 C CNN
	1    1300 4950
	0    -1   -1   0   
$EndComp
$Comp
L SCHOTTKY-DIODESMD D?
U 1 1 4F7EBEBE
P 1300 4450
AR Path="/4F7EBD43" Ref="D?"  Part="1" 
AR Path="/4F7EBEBE" Ref="D5"  Part="1" 
F 0 "D5" H 1210 4525 50  0000 L BNN
F 1 "SCHOTTKY" H 1210 4315 50  0000 L BNN
F 2 "diode-SMB" H 1300 4600 50  0001 C CNN
	1    1300 4450
	0    -1   -1   0   
$EndComp
$Comp
L AGND #PWR029
U 1 1 4F7EBEBD
P 1100 4900
F 0 "#PWR029" H 1100 4900 40  0001 C CNN
F 1 "AGND" H 1100 4700 50  0000 C CNN
	1    1100 4900
	1    0    0    -1  
$EndComp
$Comp
L PINHD-1X2 JP3
U 1 1 4F7EBEBC
P 750 4800
F 0 "JP3" H 500 5025 50  0000 L BNN
F 1 "to POT 1M" H 500 4600 50  0000 L BNN
F 2 "pinhead-1X02" H 750 4950 50  0001 C CNN
	1    750  4800
	-1   0    0    -1  
$EndComp
$Comp
L +3.3V #PWR030
U 1 1 4F7EBEBB
P 1300 4250
F 0 "#PWR030" H 1300 4210 30  0001 C CNN
F 1 "+3.3V" H 1300 4360 30  0000 C CNN
	1    1300 4250
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR031
U 1 1 4F7EBEBA
P 1300 5150
F 0 "#PWR031" H 1300 5150 40  0001 C CNN
F 1 "AGND" H 1300 5050 50  0000 C CNN
	1    1300 5150
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR032
U 1 1 4F7EBEB9
P 1500 4550
F 0 "#PWR032" H 1500 4550 40  0001 C CNN
F 1 "AGND" H 1550 4450 50  0000 C CNN
	1    1500 4550
	1    0    0    -1  
$EndComp
$Comp
L R R7
U 1 1 4F7EBEB8
P 2250 4200
F 0 "R7" V 2330 4200 50  0000 C CNN
F 1 "470K" V 2250 4200 50  0000 C CNN
	1    2250 4200
	0    1    1    0   
$EndComp
$Comp
L R R4
U 1 1 4F7EBEB7
P 1750 4500
F 0 "R4" V 1830 4500 50  0000 C CNN
F 1 "470K" V 1750 4500 50  0000 C CNN
	1    1750 4500
	0    1    1    0   
$EndComp
$Comp
L TLC274D IC1
U 3 1 4F7EBEB6
P 2300 4600
F 0 "IC1" H 2400 4725 50  0000 L BNN
F 1 "TLC274D" H 2400 4400 50  0000 L BNN
F 2 "linear2-SO14" H 2300 4750 50  0001 C CNN
	3    2300 4600
	1    0    0    -1  
$EndComp
$Comp
L TLC274D IC1
U 2 1 4F7EBEA1
P 2300 3400
F 0 "IC1" H 2400 3525 50  0000 L BNN
F 1 "TLC274D" H 2400 3200 50  0000 L BNN
F 2 "linear2-SO14" H 2300 3550 50  0001 C CNN
	2    2300 3400
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 4F7EBEA0
P 1750 3300
F 0 "R3" V 1830 3300 50  0000 C CNN
F 1 "470K" V 1750 3300 50  0000 C CNN
	1    1750 3300
	0    1    1    0   
$EndComp
$Comp
L R R6
U 1 1 4F7EBE9F
P 2250 3000
F 0 "R6" V 2330 3000 50  0000 C CNN
F 1 "470K" V 2250 3000 50  0000 C CNN
	1    2250 3000
	0    1    1    0   
$EndComp
$Comp
L AGND #PWR033
U 1 1 4F7EBE9E
P 1500 3350
F 0 "#PWR033" H 1500 3350 40  0001 C CNN
F 1 "AGND" H 1550 3250 50  0000 C CNN
	1    1500 3350
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR034
U 1 1 4F7EBE9D
P 1300 3950
F 0 "#PWR034" H 1300 3950 40  0001 C CNN
F 1 "AGND" H 1300 3850 50  0000 C CNN
	1    1300 3950
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR035
U 1 1 4F7EBE9C
P 1300 3050
F 0 "#PWR035" H 1300 3010 30  0001 C CNN
F 1 "+3.3V" H 1300 3160 30  0000 C CNN
	1    1300 3050
	1    0    0    -1  
$EndComp
$Comp
L PINHD-1X2 JP2
U 1 1 4F7EBE9B
P 750 3600
F 0 "JP2" H 500 3825 50  0000 L BNN
F 1 "to POT 1M" H 500 3400 50  0000 L BNN
F 2 "pinhead-1X02" H 750 3750 50  0001 C CNN
	1    750  3600
	-1   0    0    -1  
$EndComp
$Comp
L AGND #PWR036
U 1 1 4F7EBE9A
P 1100 3700
F 0 "#PWR036" H 1100 3700 40  0001 C CNN
F 1 "AGND" H 1100 3500 50  0000 C CNN
	1    1100 3700
	1    0    0    -1  
$EndComp
$Comp
L SCHOTTKY-DIODESMD D?
U 1 1 4F7EBE99
P 1300 3250
AR Path="/4F7EBD43" Ref="D?"  Part="1" 
AR Path="/4F7EBE99" Ref="D3"  Part="1" 
F 0 "D3" H 1210 3325 50  0000 L BNN
F 1 "SCHOTTKY" H 1210 3115 50  0000 L BNN
F 2 "diode-SMB" H 1300 3400 50  0001 C CNN
	1    1300 3250
	0    -1   -1   0   
$EndComp
$Comp
L SCHOTTKY-DIODESMD D?
U 1 1 4F7EBE98
P 1300 3750
AR Path="/4F7EBD43" Ref="D?"  Part="1" 
AR Path="/4F7EBD8E" Ref="D?"  Part="1" 
AR Path="/4F7EBE98" Ref="D4"  Part="1" 
F 0 "D4" H 1210 3825 50  0000 L BNN
F 1 "SCHOTTKY" H 1210 3615 50  0000 L BNN
F 2 "diode-SMB" H 1300 3900 50  0001 C CNN
	1    1300 3750
	0    -1   -1   0   
$EndComp
$Comp
L SCHOTTKY-DIODESMD D?
U 1 1 4F7EBD8E
P 1300 2550
AR Path="/4F7EBD43" Ref="D?"  Part="1" 
AR Path="/4F7EBD8E" Ref="D2"  Part="1" 
F 0 "D2" H 1210 2625 50  0000 L BNN
F 1 "SCHOTTKY" H 1210 2415 50  0000 L BNN
F 2 "MiniMelf" H 1160 2675 60  0001 C CNN
	1    1300 2550
	0    -1   -1   0   
$EndComp
$Comp
L SCHOTTKY-DIODESMD D1
U 1 1 4F7EBD43
P 1300 2050
F 0 "D1" H 1210 2125 50  0000 L BNN
F 1 "SCHOTTKY" H 1210 1915 50  0000 L BNN
F 2 "diode-SMB" H 1300 2200 50  0001 C CNN
	1    1300 2050
	0    -1   -1   0   
$EndComp
$Comp
L AGND #PWR037
U 1 1 4F7EBBCA
P 1100 2500
F 0 "#PWR037" H 1100 2500 40  0001 C CNN
F 1 "AGND" H 1100 2300 50  0000 C CNN
	1    1100 2500
	1    0    0    -1  
$EndComp
$Comp
L PINHD-1X2 JP1
U 1 1 4F7EBBBC
P 750 2400
F 0 "JP1" H 500 2625 50  0000 L BNN
F 1 "to POT 1M" H 500 2200 50  0000 L BNN
F 2 "pinhead-1X02" H 750 2550 50  0001 C CNN
	1    750  2400
	-1   0    0    -1  
$EndComp
$Comp
L +3.3V #PWR038
U 1 1 4F7EB92F
P 1300 1850
F 0 "#PWR038" H 1300 1810 30  0001 C CNN
F 1 "+3.3V" H 1300 1960 30  0000 C CNN
	1    1300 1850
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR039
U 1 1 4F7EB90A
P 1300 2750
F 0 "#PWR039" H 1300 2750 40  0001 C CNN
F 1 "AGND" H 1300 2650 50  0000 C CNN
	1    1300 2750
	1    0    0    -1  
$EndComp
$Comp
L AGND #PWR040
U 1 1 4F7EB73A
P 1500 2150
F 0 "#PWR040" H 1500 2150 40  0001 C CNN
F 1 "AGND" H 1550 2050 50  0000 C CNN
	1    1500 2150
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 4F7EB6D9
P 2250 1800
F 0 "R2" V 2330 1800 50  0000 C CNN
F 1 "470K" V 2250 1800 50  0000 C CNN
	1    2250 1800
	0    1    1    0   
$EndComp
$Comp
L R R1
U 1 1 4F7EB6C1
P 1750 2100
F 0 "R1" V 1830 2100 50  0000 C CNN
F 1 "470K" V 1750 2100 50  0000 C CNN
	1    1750 2100
	0    1    1    0   
$EndComp
$Comp
L TLC274D IC1
U 1 1 4F7EA5A8
P 2300 2200
F 0 "IC1" H 2400 2325 50  0000 L BNN
F 1 "TLC274D" H 2400 2000 50  0000 L BNN
F 2 "linear2-SO14" H 2300 2350 50  0001 C CNN
	1    2300 2200
	1    0    0    -1  
$EndComp
$EndSCHEMATC
