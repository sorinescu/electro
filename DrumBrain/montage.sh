#!/bin/bash
convert -flop top.png top_mirror.png
exec montage -tile 1x2 -geometry +0+100 -density 600x600 -units PixelsPerInch top_mirror.png bottom.png final.png