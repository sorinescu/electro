#!/bin/bash

# generate top layer (flipped horizontally)
convert -negate -flop t.png t_n_f.png

# generate bottom layer (not flipped)
convert -negate b.png b_n.png

# generate output and delete temp files
montage -tile 3x4 -geometry +0+0 -density 600x600 -units PixelsPerInch \
  t_n_f.png t_n_f.png t_n_f.png \
  t_n_f.png t_n_f.png t_n_f.png \
  b_n.png b_n.png b_n.png \
  b_n.png b_n.png b_n.png \
  driver_two_layers_stacked.png
#convert -page A4 -density 600x600 -units PixelsPerInch two_layers_stacked.png two_layers_stacked.pdf
convert driver_two_layers_stacked.png driver_two_layers_stacked.pdf
rm t_n_f.png b_n.png