#!/bin/bash

grep -v -E "^G90$" CNCIndexer.drl | grep -v -E "^G05$" | grep -v -E "^T0$" >CNCIndexer-gm.drl

trap 'echo Ctrl-C pressed !' 2

gerbmerge gerbmerge.cfg gerbmerge.layout

# disable Ctrl-C

echo "Correcting drill file"

# Add tool size definitions to tool def file
while read line; do
    if [ "$line" = "%" ]; then break; fi
    echo $line
done <CNCIndexer.drl >merged.drl

#cat merged.toollist.drl merged.drills.drl >> merged.drl
cat merged.drills.drl >> merged.drl
