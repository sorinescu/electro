#!/bin/bash

# generate top layer (flipped horizontally)
gerbv -b "#ffffff" -f "#ffffff" --dpi=600 -x png -o top.png merged.drl merged.gtl
mogrify -colorspace gray -auto-level -threshold 20% -flop top.png

# generate bottom layer (not flipped)
gerbv -b "#ffffff" -f "#ffffff" --dpi=600 -x png -o bottom.png merged.drl merged.gbl
mogrify -colorspace gray -auto-level -threshold 20% bottom.png

# generate output and delete temp files
montage -tile 1x2 -geometry +0+30 -density 600x600 -units PixelsPerInch top.png bottom.png two_layers_stacked.png
#convert -page A4 -density 600x600 -units PixelsPerInch two_layers_stacked.png two_layers_stacked.pdf
convert two_layers_stacked.png two_layers_stacked.pdf
rm top.png bottom.png