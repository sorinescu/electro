EESchema Schematic File Version 2  date Ma 18 sep 2012 19:09:31 +0300
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:mic4680
LIBS:borniers
LIBS:PowerSupply5V-cache
EELAYER 25  0
EELAYER END
$Descr A4 11700 8267
encoding utf-8
Sheet 1 1
Title "5V buck converter"
Date "18 sep 2012"
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 5650 3500 0    60   ~ 0
V_OUT  | Ra	| Rb     |\n4.95V  | 10k  | 3.3k	|\n3.4V    | 10k  | 5.6k	|
Text Notes 3900 3500 0    60   ~ 0
V_OUT=(Ra/Rb + 1)*1.23V\nRb=Ra / (V_OUT/1.23 - 1)
Connection ~ 5800 3300
Wire Wire Line
	5800 3250 5800 3300
Wire Wire Line
	6150 2800 6150 2750
Wire Wire Line
	6150 2750 5800 2750
Connection ~ 6750 3300
Wire Wire Line
	7000 2850 7000 3300
Wire Wire Line
	7000 3300 4350 3300
Connection ~ 4550 3300
Wire Wire Line
	4350 3300 4350 2850
Wire Wire Line
	7000 2650 6650 2650
Wire Wire Line
	6050 2850 6050 2650
Connection ~ 5300 3300
Wire Wire Line
	6050 3300 6050 3250
Wire Wire Line
	4800 2750 4800 2800
Wire Wire Line
	4350 2650 4800 2650
Wire Wire Line
	4550 2750 4550 2650
Connection ~ 4550 2650
Wire Wire Line
	4550 3300 4550 3150
Wire Wire Line
	5300 3300 5300 3350
Wire Wire Line
	6050 2650 5800 2650
Wire Wire Line
	6750 2750 6750 2650
Connection ~ 6750 2650
Wire Wire Line
	6750 3300 6750 3150
Connection ~ 6050 3300
Wire Wire Line
	6650 2650 6650 2800
Connection ~ 6650 2650
Connection ~ 5800 2750
$Comp
L R R1
U 1 1 505898AD
P 5800 3000
F 0 "R1" V 5880 3000 50  0000 C CNN
F 1 "Rb" V 5800 3000 50  0000 C CNN
	1    5800 3000
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 50589898
P 6400 2800
F 0 "R2" V 6480 2800 50  0000 C CNN
F 1 "Ra" V 6400 2800 50  0000 C CNN
	1    6400 2800
	0    1    1    0   
$EndComp
Text Notes 3900 2450 0    60   ~ 0
V_IN is between 6V and 34V
$Comp
L GND #PWR01
U 1 1 5050A8F5
P 4800 2800
F 0 "#PWR01" H 4800 2800 30  0001 C CNN
F 1 "GND" H 4800 2730 30  0001 C CNN
	1    4800 2800
	1    0    0    -1  
$EndComp
$Comp
L CP C2
U 1 1 5050A8D8
P 6750 2950
F 0 "C2" H 6800 3050 50  0000 L CNN
F 1 "220uF/16V" H 6800 2850 50  0000 L CNN
	1    6750 2950
	1    0    0    -1  
$EndComp
$Comp
L CONN_2_V P2
U 1 1 5050A8CD
P 7350 2750
F 0 "P2" H 7270 2975 40  0000 C CNN
F 1 "V_OUT" H 7320 2930 40  0000 C CNN
	1    7350 2750
	1    0    0    -1  
$EndComp
$Comp
L CONN_2_V P1
U 1 1 5050A8BF
P 4000 2750
F 0 "P1" H 3920 2975 40  0000 C CNN
F 1 "V_IN" H 3970 2930 40  0000 C CNN
	1    4000 2750
	-1   0    0    -1  
$EndComp
$Comp
L CP C1
U 1 1 5050A7E5
P 4550 2950
F 0 "C1" H 4600 3050 50  0000 L CNN
F 1 "15uF/35V" H 4600 2850 50  0000 L CNN
	1    4550 2950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5050A7CC
P 5300 3350
F 0 "#PWR02" H 5300 3350 30  0001 C CNN
F 1 "GND" H 5300 3280 30  0001 C CNN
	1    5300 3350
	1    0    0    -1  
$EndComp
$Comp
L DIODESCH D1
U 1 1 5050A7B3
P 6050 3050
F 0 "D1" H 6050 3150 40  0000 C CNN
F 1 "B260A" H 6050 2950 40  0000 C CNN
	1    6050 3050
	0    -1   -1   0   
$EndComp
$Comp
L INDUCTOR L1
U 1 1 5050A79C
P 6350 2650
F 0 "L1" V 6300 2650 40  0000 C CNN
F 1 "68uH" V 6450 2650 40  0000 C CNN
	1    6350 2650
	0    -1   -1   0   
$EndComp
$Comp
L MIC4680 U1
U 1 1 5050A77C
P 5300 3000
F 0 "U1" H 5300 3400 60  0000 C CNN
F 1 "MIC4680" H 5300 3500 60  0000 C CNN
	1    5300 3000
	1    0    0    -1  
$EndComp
$EndSCHEMATC
