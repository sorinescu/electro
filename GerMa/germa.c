// ***********************************************************
// Project: GerMa
// Author: Sorin Otescu
// Module description: Malt germination and kilning device
// ***********************************************************

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <inttypes.h>
#include <stdbool.h>
#include <math.h>
#include <stdio.h>

#define BAUD 9600
#include <util/setbaud.h>
#include <uart_util.h>

// In case of AVR with 2 USARTs define which one we want to use
#define UART_DEV 0

// Output definitions
#define HEATER_PORT      PORTC
#define HEATER_PIN       PC1
#define START_HEATER()   HEATER_PORT |= _BV(HEATER_PIN)
#define STOP_HEATER()    HEATER_PORT &= ~_BV(HEATER_PIN)
#define MOTOR_PORT       PORTC
#define MOTOR_PIN        PC6
#define START_MOTOR()    MOTOR_PORT |= _BV(MOTOR_PIN)
#define STOP_MOTOR()     MOTOR_PORT &= ~_BV(MOTOR_PIN)
#define MOTOR_ON()       (MOTOR_PORT & _BV(MOTOR_PIN))
#define PUMP_IN_PORT     PORTC
#define PUMP_IN_PIN      PC5
#define START_PUMP_IN()  PUMP_IN_PORT |= _BV(PUMP_IN_PIN)
#define STOP_PUMP_IN()   PUMP_IN_PORT &= ~_BV(PUMP_IN_PIN)
#define PUMP_IN_ON()     (PUMP_IN_PORT & _BV(PUMP_IN_PIN))
#define PUMP_OUT_PORT    PORTC
#define PUMP_OUT_PIN     PC4
#define START_PUMP_OUT() PUMP_OUT_PORT |= _BV(PUMP_OUT_PIN)
#define STOP_PUMP_OUT()  PUMP_OUT_PORT &= ~_BV(PUMP_OUT_PIN)
#define PUMP_OUT_ON()    (PUMP_OUT_PORT & _BV(PUMP_OUT_PIN))

#if 1   // actual PCB
#define SECOND_LENGTH 1000
#define LED_GREEN_ON() do { PORTC |= _BV(PC3); PORTC &= ~_BV(PC2); } while (0)
#define LED_GREEN_OFF() do { PORTC &= ~(_BV(PC3) | _BV(PC2)); } while (0)
#define LED_RED_ON() do { PORTC |= _BV(PC2); PORTC &= ~_BV(PC3); } while (0)
#define LED_RED_OFF() do { PORTC &= ~(_BV(PC3) | _BV(PC2)); } while (0)
#else   // VMLAB
#define SECOND_LENGTH 10
#define LED_GREEN_ON() do { PORTC &= ~_BV(PC2); } while (0)
#define LED_GREEN_OFF() do { PORTC |= _BV(PC2); } while (0)
#define LED_RED_ON() do { PORTC &= ~_BV(PC3); } while (0)
#define LED_RED_OFF() do { PORTC |= _BV(PC3); } while (0)
#endif

static volatile float temp[6];
static volatile uint8_t delay_time;
static volatile uint8_t second_counter;
static volatile uint8_t second_counter_sleep_start;
static volatile uint8_t adc_channel;
static volatile bool draining;
static volatile bool germinating;
static volatile bool kilning;

// Current germination parameters
static volatile int germ_cycle_count;
static volatile int germ_motor_on_time;
static volatile int germ_motor_off_time;
static volatile int germ_water_on_time;
static volatile int germ_water_off_time;
static volatile int germ_cycle = 0;

// Mechanical switches and jumpers
// 0: WLO (Water Low)
// 1: WHI (Water High)
// 2: MODE (1: Kiln, 0: Germinate)
// 3: TEST (0: Diagnostic mode)
static volatile uint8_t switch_states[4];
#define WATER_LO_PORT PIND
#define WATER_LO_BIT  PD3
#define WATER_LO()	 (!switch_states[0])
#define WATER_HI_PORT PIND
#define WATER_HI_BIT  PD2
#define WATER_HI()	 (!switch_states[1])
#define MODE_PORT     PINC
#define MODE_BIT      PC0
#define KILN()	      switch_states[2]
#define GERMINATE()	 (!switch_states[2])
#define TEST_PORT     PINC
#define TEST_BIT      PC7
#define DIAGNOSTIC() (!switch_states[3])

// Thermistor count
#define THERMISTOR_COUNT 6

// EEPROM locations - germination
uint8_t EEMEM egerm_motor_on_time;       // number of minutes that the grains are turned
uint8_t EEMEM egerm_motor_off_time;      // number of minutes that the grains rest without turning
uint8_t EEMEM egerm_water_on_time;       // number of minutes that the grains are soaked
uint8_t EEMEM egerm_water_off_time;      // number of minutes that the grains are dry (no water in tub)
uint8_t EEMEM egerm_cycle_count;         // number of soaking cycles

// EEPROM locations - kilning

static void init_ports(void) {
    // PORTA is used for thermistor input; disable pullups (will use calibrated external ones)
    DDRA = 0;   // all inputs
    PORTA = 0;  // disable pullups


    // PORTC usage (inputs use internal pullups)
    // PC0:  in: MODE
    // PC1: out: HEATER
    // PC2: out: LED1
    // PC3: out: LED2
    // PC4: out: PUMP2
    // PC5: out: PUMP1
    // PC6: out: MOTOR
    // PC7:  in: TEST
    // PORTC usage (inputs use internal pullups)
    // PD0:  in: UART RX
    // PD1: out: UART TX
    // PD2:  in: WATERHI
    // PD3:  in: WATERLO
    DDRC = _BV(PC1) | _BV(PC2) | _BV(PC3) | _BV(PC4) | _BV(PC5) | _BV(PC6);
    PORTC = _BV(PC0) | _BV(PC7);    // enable pullups and drive outputs low

    // PORTD usage (inputs use internal pullups)
    // PD0:  in: UART RX
    // PD1: out: UART TX
    // PD2:  in: WATERHI
    // PD3:  in: WATERLO
    DDRD = _BV(PD1);
    PORTD = _BV(PD0) | _BV(PD2) | _BV(PD3);    // enable pullups and drive outputs low
}

static void start_adc(void) {
    // Voltage reference is VCC with capacitor on AREF
    // channel is ADC0..ADC7 (MUX4:0 in ADMUX)
    ADMUX = _BV(REFS0) | adc_channel;

    // Enable ADC and start conversion
    // Will trigger interrupt on conversion complete
    // Prescaler is F_CPU/128
    ADCSRA = _BV(ADEN) | _BV(ADSC) | _BV(ADIE) | _BV(ADPS0) | _BV(ADPS1) | _BV(ADPS2);
}

// ADC Conversion Complete Interrupt
// It converts the raw value into a float and triggers the conversion of the 
// next ADC channel
// Stainhardt-Hart coefficients
#define THERM_A 9.324351E-4
#define THERM_B 2.258262E-4
#define THERM_C -3.2105438E-9
#define THERM_R1 2700.0   // forms a voltage divider with the thermistor (VCC-R1-thermistor-GND)
#define THERM_R(adc_val) ((adc_val)*THERM_R1/(1024-(adc_val)))
SIGNAL(SIG_ADC) {
    uint16_t adc_val = ((uint16_t)ADCH << 8) + ADCL;
    double therm_r = THERM_R(adc_val);
    double ln_r = log(therm_r);

    // Steinhardt-Hart equation for temperature
    temp[adc_channel] = 1/(THERM_A + THERM_B * ln_r + THERM_C * ln_r*ln_r*ln_r) - 273.15;

    if (++adc_channel >= THERMISTOR_COUNT)
        adc_channel = 0;
    start_adc();
}

/*******************************************************************************
 * Below two functions are needed for compatibility with standard IO facilities
 *******************************************************************************/

static uint8_t stdio_device;

/* this function will wait (block) until there is space in output buffer */
static int u_put(char a_char, FILE *stream)
{
	while (0 == UART_putchar(*(uint8_t*) fdev_get_udata(stream), a_char))
        ;
    return 0;
}

static int u_get(FILE *stream)
{
    UART_sleep_RX(*(uint8_t*) fdev_get_udata(stream));
	return UART_get(*(uint8_t*) fdev_get_udata(stream));
}

static FILE instream = FDEV_SETUP_STREAM(NULL, u_get, _FDEV_SETUP_READ);
static FILE outstream = FDEV_SETUP_STREAM(u_put, NULL, _FDEV_SETUP_WRITE);
static FILE errstream = FDEV_SETUP_STREAM(u_put, NULL, _FDEV_SETUP_WRITE);

static void init_usart(void) {
    // First disable USART
    UCSRB = 0;

    // Configure USART baud rate (see baudrate.h)
    UBRRH = UBRRH_VALUE;
    UBRRL = UBRRL_VALUE;
#if USE_2X
    UCSRA |= (1 << U2X);
#else
    UCSRA &= ~(1 << U2X);
#endif

    // Setup 8N1 serial asynchronous mode for USART
    UCSRC = _BV(URSEL) | _BV(UCSZ1) | _BV(UCSZ0);

    // Initialize USART library
    UART_init_RX(UART_DEV);
    UART_init_TX(UART_DEV);
}

static void init_timer0(void) {
    // Reset timer
    TCNT0 = 0;
    // Set prescaler 1/1024
    TCCR0 |= 0x05;
    // Enable overflow interrupt
    TIMSK |= _BV(TOIE0);
}

static void read_germination_params(void) {
    germ_cycle_count = eeprom_read_byte(&egerm_cycle_count);
    germ_motor_on_time = eeprom_read_byte(&egerm_motor_on_time);
    germ_motor_off_time = eeprom_read_byte(&egerm_motor_off_time);
    germ_water_on_time = eeprom_read_byte(&egerm_water_on_time);
    germ_water_off_time = eeprom_read_byte(&egerm_water_off_time);
}

static void write_germination_params(void) {
    eeprom_write_byte(&egerm_cycle_count, germ_cycle_count);
    eeprom_write_byte(&egerm_motor_on_time, germ_motor_on_time);
    eeprom_write_byte(&egerm_motor_off_time, germ_motor_off_time);
    eeprom_write_byte(&egerm_water_on_time, germ_water_on_time);
    eeprom_write_byte(&egerm_water_off_time, germ_water_off_time);
}

// Germination FSM - called every minute
static void germinate(void) {
    static uint8_t time_counter = 0;
    static enum {
        IDLE,
        TURNING,
        SOAKING
    } state = IDLE;

    // Germination characteristics are kept in EEPROM
    // Read them from memory and act
    
    if (germ_cycle < germ_cycle_count) {
        germinating = true;
        ++time_counter;
        
        switch (state) {
        case IDLE:
            // See if minutes counter has reached the time to either 
            // soak or turn the grains
            if (time_counter == germ_motor_off_time) {   // start turning
                time_counter = 0;
                state = TURNING;
                START_MOTOR();
            } else if (time_counter == germ_water_off_time) {    // start soaking
                time_counter = 0;
                state = SOAKING;
                draining = false;   // start flooding; stopped automatically when water high
                START_PUMP_IN();
            }
            break;
        case TURNING:
            // See if it's time to stop the motor
            if (time_counter == germ_motor_on_time) {
                time_counter = 0;
                state = IDLE;
                STOP_MOTOR();
            }
            break;
        case SOAKING:
            // See if it's time to stop soaking
            if (time_counter == germ_water_on_time) {
                time_counter = 0;
                state = IDLE;
                draining = true;   // start draining; stopped automatically when water low
                STOP_PUMP_IN();
                START_PUMP_OUT();

                ++germ_cycle;    // soaking cycle complete; after they're all done we stop the process
            }
            break;
        }
    }
    else {
        germinating = false;
    }
}

// Kilning FSM - called every minute
static void kiln(void) {
}

// Timer Overflow ISR
// Timer period is 256*1024/F_CPU
// F_CPU(MHz)   period(ms)
// 4            65.536
// 8            32.768
// 10           26.2144
// 12           21.845
// 16           16.384
#define T0_PERIOD (256*1024.0*1000/F_CPU)
#define TIMER0_COUNT(time) ((time+T0_PERIOD-1)/T0_PERIOD)
#define TIMER0_COUNT_U8(time) ((uint8_t)TIMER0_COUNT(time))
#define TIMER0_COUNT_U16(time) ((uint16_t)TIMER0_COUNT(time))
#define TIMER0_COUNT_U32(time) ((uint32_t)TIMER0_COUNT(time))
SIGNAL(SIG_OVERFLOW0) {
    static struct {
        uint8_t old_state, new_state, count;
    } switches[4];
    static bool led1;
    static uint8_t minute_counter = 0;
    uint8_t i;

    // Keep track of seconds passed
    if (++second_counter == TIMER0_COUNT_U8(SECOND_LENGTH)) {    // should be 1000
        second_counter = 0;
        if (++minute_counter == 60)
            minute_counter = 0;
    }

    // If there's a delay_time set, decrease it every second
    if (second_counter == second_counter_sleep_start) {
        if (delay_time)
            --delay_time;
    }

    // Handle switches and debouncing
    for (i=0; i<4; ++i) {
        // Read button's state
        switch (i) {
        case 0:
            switches[i].new_state = bit_is_set(WATER_LO_PORT, WATER_LO_BIT);
            break;
        case 1:
            switches[i].new_state = bit_is_set(WATER_HI_PORT, WATER_HI_BIT);
            break;
        case 2:
            switches[i].new_state = bit_is_set(MODE_PORT, MODE_BIT);
            break;
        case 3:
            switches[i].new_state = bit_is_set(TEST_PORT, TEST_BIT);
            break;
        }

        // If remains the same, advance counter
        if (switches[i].new_state == switches[i].old_state)
            switches[i].count++;
        else // If not, reset counter
            switches[i].count=0;

        // Roll, for the next time
        switches[i].old_state = switches[i].new_state;

        // If it is stable, accept it and reset counter
        if (switches[i].count > TIMER0_COUNT_U8(100)) {
            switch_states[i] = switches[i].new_state;
            switches[i].count = 0;
        }
    }

    if (!second_counter) {
        // Handle LEDs
        if (DIAGNOSTIC()) {
        // Both leds are blinking alternately
            if (led1) {
                LED_GREEN_OFF();
                LED_RED_ON();
            } else {
                LED_RED_OFF();
                LED_GREEN_ON();
            }
        }
        else if (GERMINATE()) {
            if (MOTOR_ON() || PUMP_IN_ON() || PUMP_OUT_ON()) {
                LED_GREEN_OFF();
                if (led1)
                    LED_RED_ON();
                else
                    LED_RED_OFF();
            } else if (germinating) {
                LED_RED_OFF();
                if (led1)
                    LED_GREEN_ON();
                else
                    LED_GREEN_OFF();
            } else {    // cycle complete
                LED_RED_OFF();
                LED_GREEN_ON();
            }
        }
        else {      // kiln
        }

        led1 = !led1;
    }

    // Handle germination/kilning every minute
    if (!minute_counter) {
        if (!DIAGNOSTIC()) {
            if (GERMINATE())
                germinate();
            else     // kiln
                kiln();
        }
    }
    
        // Handle LEDs
}

static void sleep(uint8_t seconds) {
    second_counter_sleep_start = second_counter;
    delay_time = seconds;
    while (delay_time);
}

static void diagnostic(void) {
    // A diagnostic cycle tests every output
    // Turn on each device for 2 seconds
    START_HEATER();
    sleep(2);
    STOP_HEATER();

    START_MOTOR();
    sleep(2);
    STOP_MOTOR();

    START_PUMP_IN();
    sleep(2);
    STOP_PUMP_IN();

    START_PUMP_OUT();
    sleep(2);
    STOP_PUMP_OUT();
}

static const char fmt_TEMP[] PROGMEM = "TEMP: %f %f %f %f %f %f\n";
static const char fmt_GERM[] PROGMEM = "GERM: %d/%d %d %d %d %d\n";
static const char fmt_KILN[] PROGMEM = "KILN: \n";

// ***********************************************************
// Main program
//
int main(void) {
    uint8_t usart_cmd;

    init_ports();
    init_timer0();
    init_usart();

    read_germination_params();

    // Enable interrupts
    sei();

    // Prepare standard IO facilities (mapped over USART)
    stdio_device = UART_DEV;
    stdin = &instream;
    stdout = &outstream;
    stderr = &errstream;
    fdev_set_udata(stdin, &stdio_device);
    fdev_set_udata(stdout, &stdio_device);
    fdev_set_udata(stderr, &stdio_device);

    // Begin reading temperature values
    start_adc();

    while (1) {
        if (DIAGNOSTIC()) {
            diagnostic(); 
        } else {
            // Safety measures:
            // Make sure we stop the inlet pump if water level is too high
            if (WATER_HI() && !draining) 
                STOP_PUMP_IN();

            // Make sure we stop the outlet pump if water level is too low
            if (WATER_LO() && draining) 
                STOP_PUMP_OUT();
        }

        // See if we got commands on the USART
        if (UART_getchar(UART_DEV, &usart_cmd)) {
            switch (usart_cmd) {
            case 'T':   // temperatures
                printf_P(fmt_TEMP, temp[0], temp[1],
                       temp[2], temp[3], temp[4], temp[5]);
                break;
            case 'G':   // germination characteristics
                printf_P(fmt_GERM, germ_cycle, germ_cycle_count, 
                         germ_motor_on_time, germ_motor_off_time,
                         germ_water_on_time, germ_water_off_time);
                break;
            case 'g':   // set germination characteristics
                scanf_P(fmt_GERM, &germ_cycle, &germ_cycle_count, 
                         &germ_motor_on_time, &germ_motor_off_time,
                         &germ_water_on_time, &germ_water_off_time);
                write_germination_params();
                break;
            case 'K':   // kilning characteristics
                printf_P(fmt_KILN);
                break;
            }
        }
    }
}



